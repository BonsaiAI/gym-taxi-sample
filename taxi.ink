schema GameState
    Int16{0:499} location
end

schema Action
    Int8{0:5} command
end

schema TaxiConfig
    Int8 episode_length,
    Int8 num_episodes,
    UInt8 deque_size
end

simulator taxi_simulator(TaxiConfig)
    action (Action)
    state (GameState)
end

concept taxi_service is classifier
    predicts (Action)
    follows input(GameState)
    feeds output
end

curriculum taxi_services_curriculum
    train taxi_service
    with simulator taxi_simulator
    objective open_ai_gym_default_objective
        lesson pickup_dropoff
            configure
                constrain episode_length with Int8{-1},
                constrain num_episodes with Int8{-1},
                constrain deque_size with UInt8{1}
            until
                maximize open_ai_gym_default_objective
end
