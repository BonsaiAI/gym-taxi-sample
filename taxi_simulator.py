import gym

import bonsai
from bonsai_gym_common import GymSimulator

ENVIRONMENT = 'Taxi-v1'
RECORD_PATH = None


class TaxiSimulator(GymSimulator):

    def __init__(self, env, record_path, render_env):
        GymSimulator.__init__(
            self, env, skip_frame=1,
            record_path=record_path, render_env=render_env)

    def get_state(self):
        parent_state = GymSimulator.get_state(self)
        return bonsai.simulator.SimState({"location": parent_state.state},
                                         parent_state.is_terminal)

if __name__ == "__main__":
    env = gym.make(ENVIRONMENT)
    base_args = bonsai.parse_base_arguments()
    simulator = TaxiSimulator(env, RECORD_PATH, not base_args.headless)
    bonsai.run_with_url("taxi_simulator", simulator, base_args.brain_url)
